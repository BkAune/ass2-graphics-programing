#pragma once

#include <vector>

#include "ScreenObject.h"
#include "ModelHandler.h"
#include "Level.h"
#include "TextureHandler.h"

class Movable : public ScreenObject
{
public:
	enum direction
	{
		UP,
		LEFT,
		DOWN,
		RIGHT,
		NONE
	};

	Movable() {};
	Movable(Model* model, glm::vec3 position, glm::vec3 hitbox, glm::vec3 scale, std::string name, TextureHandler* textureHandler) : ScreenObject(model, position, hitbox, scale, name, textureHandler) 
	{
		m_scale = scale;
	};
	~Movable() {};

	void move(Level* currentLevel, float deltaTime);
	void rotate(glm::vec3 direction);

	bool handleCollision(Level* currentLevel);
	bool checkCollisionWith(ScreenObject* collidable);
	void resolveCollision(Level* currentLevel, ScreenObject* collidable);

	void setDirection(Movable::direction direction);
	void setSpeed(float speed);

	glm::vec3 getDirection();

private:
	direction m_direction = Movable::direction::NONE;
	direction m_oldDirection = Movable::direction::NONE;
	glm::vec3 m_movementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 m_oldMovementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	float m_speed = 0.0f;
	glm::vec3 m_scale;
};