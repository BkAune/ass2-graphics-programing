#include "ShaderHandler.h"

const std::string shaderPath = "./resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	currentShader = POINT_LIGHT;
	this->shaders["FullLighting"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("FullLighting"));
	this->shaders["SpotLight"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("SpotLight"));
	return this->shaders["FullLighting"].get(); 
}

ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = this->shaders.find(shader);
	if (it != this->shaders.end())
	{
		return this->shaders[shader].get();
		printf("Could not find shader by name: %s\n", shader.c_str());
	}

	return nullptr;
}

ShaderHandler::ShaderProgram* ShaderHandler::switchShader()
{
	std::string shader;
	if(currentShader == POINT_LIGHT)
	{
		shader = "SpotLight";
		auto it = this->shaders.find(shader);
		if (it != this->shaders.end())
		{
			currentShader = SPOT_LIGHT;
			return this->shaders[shader].get();
			printf("Could not find shader by name: %s\n", shader.c_str());
		}
	}
	else if (currentShader == SPOT_LIGHT)
	{
		shader = "FullLighting";
		auto it = this->shaders.find(shader);
		if (it != this->shaders.end())
		{
			currentShader = POINT_LIGHT;
			return this->shaders[shader].get();
			printf("Could not find shader by name: %s\n", shader.c_str());
		}
	}
	return nullptr;
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";

	this->programId = LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	
	this->MVPId = glGetUniformLocation(this->programId, "MVP");
	this->viewMatrixId = glGetUniformLocation(this->programId, "ViewMatrix");
	this->modelMatrixId = glGetUniformLocation(this->programId, "ModelMatrix");

	this->textureId = glGetUniformLocation(this->programId, "textureBuffer");

	this->lightPositionId = glGetUniformLocation(this->programId, "LightPosition_worldspace");

	this->cameraPositionId = glGetUniformLocation(this->programId, "CameraPosition_worldspace");

	this->lightColorId = glGetUniformLocation(this->programId, "lightColor");
	this->lightId = glGetUniformLocation(this->programId, "lightId");
	//this->materialDiffuseColorId = glGetUniformLocation(this->programId, "materialDiffuseColor");
	//this->materialSpecularColorId = glGetUniformLocation(this->programId, "materialSpecularColor");
	glUseProgram(this->programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(this->programId);
}