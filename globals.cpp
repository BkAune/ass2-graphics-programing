#include "globals.h"

const char gWINDOW_CONFIGURATION_FILE[] = "windowConfig";
const char gLEVELS_FILE[] = "levelsList";
const char gTEXTURE_FILE[] = "textureList";

bool gRunning = true;
float gFpsGoal = 60.0f;

std::vector<Level> gLevels;
std::vector<GLuint> gTextures;