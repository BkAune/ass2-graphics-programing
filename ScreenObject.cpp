#include "ScreenObject.h"
#include "TextureHandler.h"
#include "LightHandler.h"
#include <iostream>

//constructor for the floor of our level.
ScreenObject::ScreenObject(Model* model, glm::vec3 position, glm::vec3 hitbox, glm::vec3 scale, std::string name, TextureHandler* textureHandler)
{
	//std::printf("We create a plane for our game\n");
	m_model = model;
	m_position = position;
	m_hitbox = hitbox;
	m_modelMatrix = glm::translate(glm::mat4(1.0f), position);
	m_modelMatrix = glm::scale(m_modelMatrix, scale);
	this->textureHandler = textureHandler;
	textureName = name;
}

//Need to add the LightHandler here, so that we can pass information to the shader.
//do also need to rewrite the shader so that it takes is the new information.

void ScreenObject::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, glm::vec3 position, glm::vec3 direction)
{
	glm::vec3 lightPosition;
	glm::vec4 lightDir;
	lightDir = glm::vec4(direction,0.0f);
	lightPosition = position + glm::vec3(0,0.3f,0);



	glm::mat4 MVPMatrix = camera->getProjectionMatrix() * camera->getViewMatrix() * m_modelMatrix;

	glm::vec4 lightColor = glm::vec4(0.8f,0.5f,0.2f,1.f);

	textureHandler->setActiveTexture(textureName);

	//binds shaderprogram
	glUseProgram(shaderProgram->programId);
	
	//bind vertex array
	glBindVertexArray(m_model->VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_model->IBO);

	//send information to the shader.
	glUniformMatrix4fv(shaderProgram->MVPId, 1, GL_FALSE, glm::value_ptr(MVPMatrix));
	glUniformMatrix4fv(shaderProgram->viewMatrixId, 1, GL_FALSE, glm::value_ptr(camera->getViewMatrix()));
	glUniformMatrix4fv(shaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(m_modelMatrix));

	//
	glUniform4f(shaderProgram->lightPositionId,	lightPosition.x, lightPosition.y, lightPosition.z, 1.0f);
	glUniform4f(shaderProgram->lightId, lightDir.x, -lightDir.z, -lightDir.y, lightDir.w);	//For some reason, light directions are messing up. Y=Z, Z=Y
	glUniform1i(shaderProgram->textureId, 0);

	glUniform4fv(shaderProgram->lightColorId, 1, glm::value_ptr(lightColor));
	

	

	//does the actual drawing
	glDrawElements(GL_TRIANGLES, m_model->numberOfIndices, GL_UNSIGNED_INT, NULL);

	glUseProgram(NULL);

}

void ScreenObject::update()
{

}

//GET/////////////////////////////////////////
glm::mat4 ScreenObject::getModelMatrix()
{
	return m_modelMatrix;
}

glm::vec3 ScreenObject::getPosition()
{
	return m_position;
}

glm::vec3 ScreenObject::getHitbox()
{
 	return m_hitbox;
}

//SET/////////////////////////////////////////
void ScreenObject::setModel(Model* model)
{
	m_model = model;
}

void ScreenObject::setModelMatrix(glm::mat4 modelMatrix)
{
	m_modelMatrix = modelMatrix;
}

void ScreenObject::addTextures(GLuint& texture)
{
	textures.push_back(texture);
}

void ScreenObject::updatePosition()
{
	m_modelMatrix = glm::translate(glm::mat4(1.0f), m_position);
}