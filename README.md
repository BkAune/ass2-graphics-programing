# Assignment 2

## Group creation deadline 2016/11/11 23:59:59
## Hand in deadline 2016/11/25 23:59:59

In this assignment you will be making Pac Man in 3D.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

There has been som miscomunication between Simon myself and the examination office. The assignments are supposed to be 40% of your grade (not 60% as I though). As such this assignment is counting towards 20% of your grade.  
You will have 3 weeks to finish this assignment.

## Required work
2. Finish the Level::createWalls() function so that walls are created. (The walls should be in 3D)
  2. In addition add a floor to the level.
1. Finish ModelHandler::createModel()
  2. You might wish to change the Model struct.
3. Add textures to the walls. (Any kind of texture will do as long as it is visible. Preferably not yellow.)
3. Finish the Camera class.
  3. The camera should be placed at a 60 degree angle to the level with a perspective view.
4. Finish the Movable::move() function.
  5. Move over any changes you made to the InputHandler in assignment 1 that you think you will need.
5. Create a pacman object from the Movable class or a subclass thereof. (It is ok if pacman is a cube.)
  5. Pacman should be controllable using WASD. He should be able to move in 2D. (You might want to change the Movable::setDirection() function to be more intuative for you.)
  5. Pacman should be yellow
6. Give the packman object a light. The light should have a yellow/redish color. Like from a torch.
  3. Add different kinds of light so that pacman can switch between a pointlight and a spotlight pointing forward by pressing the E key.
7. Create shaders that draw the map in darkness unless pacman is there with his light. The lighting should be full phong lighting and take the color of pacmans light into account. (There is no need to implement shadows. The light can go through the walls.)
2. The code should compile and run on Linux.
1. Update this document. (See bottom.)

## Notes
1. I forgot I promised to add collision to the skeleton code. I'll finish it by the end of the weekend. You are welcome to make your own if you don't want to wait.
2. If you have the same issue in your code that exist in the labs where the code will produce a segmentation fault unless being run in debug mode add a comments about that. There will be no penalty if this is the case.
3. The base code has not been tested on Windows.
4. If there are any bugs in the code or other issues please send me an e-mail about it. I have not added any bugs on purpos.

## Suggestions for additional work
1. Implement loding of OBJ files and load and use a model from an OBJ file.
2. Implement a way to move the camera.
4. Port over features from assignment 1. (This will not give as much credit as in asignment 1 unless heavy modification was needed. Add comments about that you needed to change below.)
6. Add a way to switch between perspective and orthographic projection.
3. Add shadows.

##Group comments
###Who are the members of the group?###
Members of the group are:

Bj�rn Kaare Aune

Hans Emil Eid

Benjamin Normann Skinstad

###What did you implement and how did you do it? (Individually)###
For the most part, we all worked together on most of our project, with different members 
doing different work on the end product.

Benjamin worked on getting text to our screen, as well as bugtesting. 

Hans made the ghosts, the "game" parts, and the various cameras + the orthographic projection, and bugfixing.

Bj�rn fixed shading and switching between lights, as well as working out some of the framework in the first place.

###What parts if any of the base code did you change and why?###
We added things to the inputhandler, so that we had enums for all our keys. The texturehandler wouldn't work out of the box, so that had to go through some significant changes. A few changes in movable.

###What was the hardest part of this assignment? (Individually)###
Hans: Being stuck on the texturehandler was a pain, but ultimately not "hard", since once we found the (slightly too obvious) problem, we resolved it in a matter of minutes. The obj-loader has definitely been the biggest challenge for me, which is why it's not actually implemented.

Benjamin: I had problems with getting glewt/opengl to work, my skills with it comes to makefiles is insanly bad, but i became better at it later. finding out how to get 2d text to work was insanly hard, it was so many ways to incorporate it, and most of them did not work with opengl4. I did manage to get it to work after a lot of work.

###Did you feel like the assignment was an appropriate amount of work? (Individually)###
Bj�rn: Yes. We worked for a long time on some of the features. And the ones we didn't want to 
implement was because of us not having enough time. Examples are: obj loader, multiple lights, and AIs for our ghosts.

Hans: Definitely an appropriate amount of work. Would've been nice if we hadn't been stuck on some bugs for way too long, but that's on us, not the assignment.

Benjamin: yea, the required work was nice, and then we had the opperunity to chose what to add to the project. 

###What additional features if any did you add?###
We added a shader that makes ghosts and pacman have a light inside them, so that thy are illuminated from within. 
Unfortunately, the light in our ghosts do not light up the world around them, but only themselves.

It's an actual game at this point too; if you eat all the candies the level 'resets' but with score and lives staying the same and a level counter that goes up, you have collision, ghosts that move about and kill you, candies, cheeses that turn you into superpac for a short while which lets you eat ghosts, and a game over screen.

The HUD was one of the last features we managed to finalize, and it works well.

There's an orthographic view mode (accessed by pressing 'f' in-game, wouldn't recommend using it with First Person-mode), and we've set up three different cameras which are toggled between with 'e' (top-down, which is optimal in orthographic, angled, which is what was required, and first person)

###Are there any keybindings I should be aware of ouside of WASD?###
All our keybindings are written in our game's HUD

###Other comments###
Some code for the obj-loader is in the objLoader branch, specifically the code in the Mesh and Model files, but ultimately (unless we miraculously managed to do it by midnight) it didn't work. Might be worth taking a look at though.
we also wanted to add lights to the ghosts to make them light up, sadly we did not get this to work.

Slight problem with the spotlight; Going up or down it's angled at about 45 degrees. Going up (or forward) it's angled 45 degrees towards the sky, down it's 45 towards the ground. This doesn't happen in top-down, so it probably has something to do with the up-vector or generally what the shader considers to be up. Also a problem in first person, but the light just straight up doesn't show up there anyway.

Couldn't get rotate to work properly, tried some things but don't have the code saved anywhere since it just wouldn't work. It'd smoothly rotate to a certain degree, then stop rotating forever.

We would've liked to have a bit of a proper eventqueue as well (pressing left while hugging a left wall would make you move left at the first possible opening), but that didn't happen. Right now you gotta be really accurate when aiming yourself into each wall. I'd suggest top-down orthographic, it's much easier to calculate when to turn.

Bet you can't beat level 1 in first person by the way.