#include "Camera.h"
#include "WindowHandler.h"

Camera::Camera(glm::vec3 position)
{
	m_position 			= position;

	lookAt();
	setProjection(0.1f, 200.0f);
}

Camera::~Camera()
{

}

/**
 * Sets the viewMatrix for the camera
 * @param position 	The position the camera should look at
 * @param up  		The up direction
 */
void Camera::lookAt(glm::vec3 position, glm::vec3 up)
{
	m_viewMatrix = glm::lookAt(
		m_position, // Camera position in World Space
		position, // and looks at the origin
		up  // Head is up (set to 0,-1,0 to look upside-down)
		);
}

/**
 * Sets the position of the camera
 * @param position 
 */
void Camera::setPosition(glm::vec3 position)
{
	m_position = position;
}

/**
 * Sets the perspective matrix for the camera
 * @param near Near clip plane
 * @param far  Far clip plane
 * @param fov  Field of view angle in radians
 */
void Camera::setProjection(float near, float far, float fov)
{
	glm::vec2 screenSize;
	screenSize = WindowHandler::getInstance().getScreenSize();
	m_projectionMatrix = glm::perspective(glm::quarter_pi<float>() * 1.5f,
		screenSize.x / screenSize.y,
		0.1f, 200.0f);
}

glm::mat4 Camera::getViewMatrix() {
	return m_viewMatrix;
}

glm::mat4 Camera::getProjectionMatrix() {
	return m_projectionMatrix;
}

glm::vec3 Camera::getPosition()
{
	return m_position;
}