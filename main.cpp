#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>
#include <iostream>

#include "globals.h"

#include "InputHandler.h"
#include "WindowHandler.h"
#include "ShaderHandler.h"
#include "ModelHandler.h"
#include "TextureHandler.h"
#include "LightHandler.h"

#include "Level.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Movable.h"
#include "Camera.h"



//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels(ModelHandler* modelHandler, TextureHandler* textureHandler)
{
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) {
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString, modelHandler, textureHandler);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

void bindTexture(int i)
{
	glActiveTexture(GL_TEXTURE0 + i);
	glBindTexture(GL_TEXTURE_2D, gTextures.at(i));
}

bool initGL()
{
	//Success flag
	bool success = true;

	//	Enable culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	return success;
}

/**
 * Initializes the InputHandler, WindowHandler and OpenGL
 */
bool init()
{
	InputHandler::getInstance().init();

	if(!WindowHandler::getInstance().init()) {
		gRunning = false;
		return false;
	}

	initGL();

	return true;
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Level* currentLevel, Movable* pacman,
					ShaderHandler* shaderHandler, ShaderHandler::ShaderProgram*& currentShader)
{
	GameEvent nextEvent;
	while(!eventQueue.empty())
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		switch(nextEvent.action) {
			case ActionEnum::PLAYER_MOVE_UP:
				pacman->setDirection(Movable::direction::UP);
				std::cout << "up\n";
				break;
			case ActionEnum::PLAYER_MOVE_LEFT:
				pacman->setDirection(Movable::direction::LEFT);
				std::cout << "funk\n";
				break;
			case ActionEnum::PLAYER_MOVE_DOWN:
				pacman->setDirection(Movable::direction::DOWN);
				std::cout << "down\n";
				break;
			case ActionEnum::PLAYER_MOVE_RIGHT:
				pacman->setDirection(Movable::direction::RIGHT);
				std::cout << "you\n";
				break;
			case ActionEnum::LIGHT_SWITCH:
				 currentShader = shaderHandler->switchShader();
				break;
			default:
				break;
		}
	}

	pacman->move(currentLevel, deltaTime);
}

//All draw calls should originate here
void draw(Level* currentLevel, Camera* camera, ShaderHandler* shaderHandler,
	TextureHandler* textureHandler, Movable* pacman, ShaderHandler::ShaderProgram* currentShader)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	
	for(auto wall : (*currentLevel->getWalls()))
	{
		
		wall.draw(camera, currentShader, pacman->getPosition(), pacman->getDirection());
	}
	currentLevel->getPlane()->draw(camera, currentShader, pacman->getPosition(), pacman->getDirection());
	pacman->draw(camera, currentShader, pacman->getPosition(), pacman->getDirection());

	//Update screen
	SDL_GL_SwapWindow(WindowHandler::getInstance().getWindow());
}

//Calls cleanup code on program exit.
void close()
{
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[])
{
	float nextFrame      = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime      = 0.0f; //Time since last pass through of the game loop.
	auto clockStart      = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop       = clockStart;

	init();

	ModelHandler modelHandler;
	modelHandler.loadModelData("cube", "./resources/models/cube.model");
	
	TextureHandler textureHandler;
	
	GLuint wallTexture = textureHandler.createTextureFromImage("./resources/textures/wall.bmp","wall");
	GLuint planeTexture = textureHandler.createTextureFromImage("./resources/textures/plane.bmp","plane");
	GLuint pacTexture = textureHandler.createTextureFromImage("./resources/textures/yellow.bmp","pac");
	
	gTextures.push_back(wallTexture);
	gTextures.push_back(planeTexture);
	gTextures.push_back(pacTexture);

	ShaderHandler shaderHandler;
	shaderHandler.initializeShaders();
	ShaderHandler::ShaderProgram* currentShader;
	currentShader = shaderHandler.getShader("FullLighting");

	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	
	Level* currentLevel = loadLevels(&modelHandler, &textureHandler);

	Movable pacman(modelHandler.getModel("cube"), currentLevel->getStartPos(), glm::vec3(0.8f,0.8f,0.8f), glm::vec3(0.5f,0.5f,0.5f), "pac", &textureHandler); 
	pacman.setSpeed(0.005f);

	

	Camera mainCamera(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y+20, 31.0f));
	mainCamera.lookAt(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y-50.f, 20.f), glm::vec3(0.0f, 1.0f, 0.0f));

	for (int i = 0; i < currentLevel->getWalls()->size(); ++i)
	{

		currentLevel->getWalls();
	
	}

	currentLevel->getPlane();



	while(gRunning)
	{
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		
		update(deltaTime, eventQueue, currentLevel, &pacman, &shaderHandler, currentShader);

		if(nextFrameTimer >= nextFrame)
		{			

			draw( currentLevel, &mainCamera, &shaderHandler, &textureHandler, &pacman, currentShader);
			nextFrameTimer = 0.0f;


		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::milliseconds::period>(clockStop - clockStart).count();
		
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}